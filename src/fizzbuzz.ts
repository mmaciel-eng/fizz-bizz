import { range } from "rxjs";
import { map } from "rxjs/operators";

function fizz(value: number, fn: any) {
  if (value % 3 === 0) {
    return fn(value, "Fizz");
  }

  return fn(value, "");
}

function buzz(value: number, currentValue: string) {
  if (value % 5 === 0) {
    return currentValue + "Buzz";
  }

  return currentValue === "" ? value : currentValue;
}

export function fizzbuzz(counter: number) {
  if (counter <= 0) {
    throw new Error("Invalid range!");
  }

  range(1, counter)
    .pipe(map(value => fizz(value, buzz)))
    .subscribe(result => console.log(result));
}
